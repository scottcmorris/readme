# Scott's README
Hello. My name is Scott Morris and my pronouns are he/him/his. I look forward to getting to know you; but if we already know eachother, then I look forward to continuing our relationship! Either way, hopefully this document helps you better understand who, what, and how I am as a manager, a teammate, and a person.

## Expectations
#### Promises to My Team
* I will be transparent and I am going to always tell you the truth (or half-truth for those few things that I have to legally keep a secret).
* Fairness is really important to me; I will fiercely fight for fair expectations, treatment, and opportunities for everyone -- but especially the team.
* You can consider our private conversations as confidential unless otherwise specified, or I have an obligation to report for legal or safety reasons.
* If there is something important you need to talk about, ask and I will make time for you as soon as possible.

#### In Return
* Above all else, I need you to be honest and transparent with me. 
* I expect everyone to actively share with one another -- be it knowledge, learnings, code, or responsibilities. My first thought when encountering a problem is -- who do I think would be best equipped to solve this? So don't be surprised if I route you to someone else or route someone to you to help problem-solve!
* Own your mistakes. Everyone makes mistakes and everyone royally effs up occasionally -- me more than others -- and I don't expect you to be any different. But I do firmly believe that mistakes can be healthy and the only failures are those we refuse to admit to or learn from.
* Communicate with me and with the team. Car broke down and you can't make it to work? No problem, just let me know. Need to cancel a meeting last minute? Understandable, just let the fellow attendees know. Struggling for hours with a problem? Ask a teammate or me!

## Communication
#### Channels
For synchronous conversations, I prefer speaking in person or via Zoom.

For asynchronous communication, I prefer Slack, email, and Jira -- in that order. My response time follows the same order, but even with Slack, I cannot guarantee that I'll respond immediately. Especially now with working remotely, I tend to engage more during meetings and frequently lose track of Slack notifications. But I promise that I always check for new Slack messages after every meeting. 

#### Schedule
Everyone has their own schedule that works best for them, so I prefer not to prescribe a schedule as long as you put in the hours needed to get everything done.
 
Personally, I typically work a 9 - 5. My WFH schedule includes a couple breaks for helping with the family and making time for lunch, but I plug in most nights to finish up work in the evening.

#### One-on-ones
I will schedule at least 30-minutes weekly for one-on-ones. One-on-ones are **your** time and are not for project status reports, so the last thing I want to talk about is how Project A is going. What I *do* want to talk about are topics like progress-through-level, career development, ideas or concerns you may have, how you're enjoying your work, philosophy, hobbies, good startup ideas, etc.

#### Calendar
Feel free to book time on my calendar whenever there's availability. 

I try to keep my calendar as bare as possible by minimizing unnecessary meetings and actively excusing myself from meetings I no longer need to attend. I also schedule my breaks, so don't ever worry about taking up a timeslot. However, my calendar may still fill up from time to time, so if you have something you want to talk with me about, let me know and I will make the time. 

There are very few meetings that are so important that they cannot be rescheduled.

#### Feedback
My default preference for providing positive feedback is immediately via Slack, email, or sometimes during a team meeting to celebrate the achievement/accolade/recognition. 

My default preference for providing critical feedback is to share it with you during our regularly scheduled 1:1s. However, if there's any urgency to the feedback, I will schedule an earlier 1:1 meeting. Note that I also randomly schedule urgent 1:1s to share private information like, "Hey, your bonus this quarter is $X," so don't panic if I put some time on your calendar separate from our regular 1:1.

I know my style isn't for everyone, so if you prefer to receive feedback in any other way -- immediately, via email, not infront of your peers, only on Tuesdays, whatever your preference is -- please let me know and I will gladly accomodate. 

## Quirks & Philosophies
#### Strong Opinions, Loosely Held
I am very passionate about some things, and that passion can appear to manifest itself as stubbornness or overbearingness. If a conversation continues long enough and I have yet to convince you to agree with me, I may start to continuously rephrase my opinions in an endless loop hoping to win you over.

Note that I am not a stubborn person, however! I am also always seeking to update my [mental models](https://en.wikipedia.org/wiki/Mental_model), so please know that if I am ever sharing my opinion with you, I am inviting you to offer an alternative perspective.

But if I get too passionate, please do call me out out on it. 

#### Empowerment
The thing I look forward to the most as a manager is finding your areas of interests and working together towards your growth or progress in those areas. Whether you want to dive deeper into the career you've begun, change course completely, become a better public speaker, or learn a new language, it is my job to help you get there!

#### Work/Life Balance
The balance between personal and professional life is incredibly individual -- some people enjoy spending their free-time continuing career development and others turn off as soon as they *leave the office*, whatever that looks like nowadays. 

I fall somewhere in-between. It's really important for me to check out of work when I clock out, in order to focus on my family. But if I haven't finished everything, I like to clock back in at night once the kids fall asleep. So don't be surprised if I'm seemingly ignoring a message from you in the early morning or evening, or also if I'm sending a message at 1:00 am. 

And know that it's just as important to me that you have the same respect as well, whatever your balance is. There is the occasional need to sacrifice personal time for work. However, nearly everything we do as analytics professionals can be planned for, so there those occasions should be extremely rare and never surprising. And above all, the physical, mental, and emotional well-being of you and yours is more important than anything we are responsible for at work, so please talk with me if you feel overwhelmed, stressed, or burned out.

#### Diversity
Diversity, inclusion, and belonging are really important to me. And while I do recognize the value of diversity in race and gender, I am even more interested in diversity of experiences, personalities, and cultures! I truly believe that it's the diversity of a team that strengthens it, so any time you feel you can offer a different perspective, I encourage you to share it.

## Personal Life
If you're ever at a lost for words in a conversation with me, and by some freak miracle I am not already blabbing away, feel free to bring up any of the topics below. They're all guaranteed to get me chatting.

#### Personality
I am an introvert. I know; I can seem super chatty at the office, but that's out of necessity and is not my default personality! I share this so that you're aware that the worst time to meet with me is in the afternoon after a day full of meetings, once I'm socially exhausted. 

For those of you who like personality tests, I've found my results to be pretty accurate from the following evaluations:
* [INFJ](https://www.16personalities.com/infj-personality) (Myers-Briggs)
* Supporter/GBYR([Insights Discovery](https://www.thecolourworks.com/resources/getting-the-most-out-of-your-profile/))
* Intellection - Developer - Empathy - Strategic - Futuristic([Strengths Finder](https://www.gallup.com/cliftonstrengths/en/253715/34-cliftonstrengths-themes.aspx))

#### Family
I grew up in Austin, TX and only took a break from the city twice: once when I enlisted in the Air Force and once to attend Texas A&M in College Station, TX. During a two-year deployment to Korea, I met my wife and we married in Busan before returning to Austin after our honeymoon. Now we live in west Austin with our two sons, Rowan and Robin (born 2017 & 2019, respectively).

#### Interests
**Gaming:** I'm a gamer. I play tons of video games, card games, and board games; although lately I have little time to play anything before the kids go to sleep. Still, I've been finding the time recently to play Star Wars Squadrons (PC) and Gloomhaven (on Tabletop Simulator).

**Soccer:** First off, I suck at actually playing, but I love to watch the sport. I primarily follow the English Premier League and typically catch 3 - 4 games per weekend during the season, as well as Europe games midweek. I'm a Manchester City fan, and I love some good banter and tactics discussion.

**Electric Vehicles:** I have owned two electric cars -- a BMW i3 and Tesla model 3 -- and I enjoy researching and talk about the future of the industry. I'm especially keeping an eye on the progress of [VW ID.Buzz](https://commons.wikimedia.org/wiki/File:Volkswagen_I.D._Buzz_at_IAA_2019_IMG_0780.jpg#/media/File:Volkswagen_I.D._Buzz_at_IAA_2019_IMG_0780.jpg), [Tesla Cybertruck](https://www.tesla.com/cybertruck), and [EV two-wheelers](https://www.vespa.com/us_EN/vespa-models/vespa-elettrica.html).

**Music:** I played the [euphonium](https://youtu.be/Aqw8v1ILB2g?t=67) in high school and quit for 2 decades before COVID-19 encouraged me to pick up the instrument again and relearn how to play. 

**Longboarding:** I've recently picked up longboarding in order to ride alongside my oldest son while he learned to ride a scooter (and now skateboard!). While watching Youtube videos to learn how to ride better, I ran across [longboard dancing](https://youtu.be/afQTu5S1tEc) and fell in love. I'm still an amateur and can only do a handful of steps, but I'm learning!

**Korea:** My entire 4-year Air Force career was dedicated to Korea: I learned Korean to fluency in a year and a half and spent 2 years in the country. I love the country, its language, its people, its food, its music, its media, and more. Little known fact: had I not married, I may never have left the country as I had been accepted to an undergraduate program at Korea University. Note that I'm not a K-POP stan; most of the Korean music I listen to is older ([Harim](https://open.spotify.com/album/6X16lArMkuAc9vpCi5M0n6), [YB](https://open.spotify.com/album/4S5PRo1gVG9BvRnCcdYzdS), [Epik High](https://open.spotify.com/album/0dcpWdZ9nKIbxgZma4NhMa)), but I'm always happy to add new songs to my playlists.

**Coffee:** I'm not a coffee snob, but I love hand-ground, pour-over coffee and espresso! My current pour-over setup is a [Kinu M47](https://www.kinugrinders.com/), [Chemex](https://www.chemexcoffeemaker.com/coffeemakers.html) with Chemex filters for 2+ cups or [Bee House ceramic dripper](https://prima-coffee.com/equipment/zero-japan/bkk-15l-wh) with Melitta for 1 cup pours. My current espresso setup uses the same grinder with a [Breville BES870XL](https://www.seattlecoffeegear.com/media/catalog/product/cache/1/small_image/9df78eab33525d08d6e5fb8d27136e95/b/a/barista_express_main_image.jpg) machine. I've been very happy with Ruta Maya beans (from Costco!) for both pour overs (dark roast) and espresso (espresso roast), but I'm always open to coffee bean recommendations!

#### Career
My career has been anything but straight-forward, so don't bother looking for a pattern here. In order from most to least recent:
* Business Intelligence @ Indeed
* Sales Operations @ Omnitracs
* MBA @ Texas A&M University
* Business Operations @ Rackspace
* IT Support @ Blackbaud
* Computer Systems Management @ St. Edward's University
* Technical Support @ Blizzard Entertainment
* SIGINT Analyst, Korean Specialist @ United States Air Force
* Korean @ Defense Language Institute

## Contribute
This is in no way a perfect document and I plan on continuously editing it -- no promises, though!

That said, if I've written anything that is incorrect or there's any areas you want to contribute to, feel free to submit a merge request! The project is public and I promise to review all merge requests.

*Last updated 2020.10.26*
